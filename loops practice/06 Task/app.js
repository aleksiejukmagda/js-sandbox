//1 
const makeNewString = (str) => 
str.length < 3 ? str : str.slice(0, 3) + str.slice(-3);


console.log(makeNewString('magdaaleksiejuk'));
console.log(makeNewString('ma'));


//2
const firstHalf = (str) => 
str.slice(0, str.length / 2);

console.log(firstHalf('magdaaleksiejuk'));


//3
const createNewString = (str1, str2) =>
str1.slice(1) + str2.slice(1);

console.log(createNewString('magda', 'aleksiejuk'));


//4 
const closeTo100 = (a, b) =>
Math.abs(100 - a) < Math.abs(100 - b) ? a : b;

console.log(closeTo100(70, 2000));

//5

const countChars = (str, char) => 
str.split('').filter(ch =>  ch === char).length;

const contains2To4 = (str, char) => 
countChars(str, char) >= 2 && countChars(str, char) <= 4;

console.log(contains2To4('magdalenaaleksiejuk', 'a'))