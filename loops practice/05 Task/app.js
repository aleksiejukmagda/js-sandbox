//1 
const checkNum = (num1, num2) =>  num1 === 100 || num2 === 100 || (num1 + num2) === 100; 

console.log(checkNum(5, 95));


//2 
const getFileExt = (str) => str.slice
(str.lastIndexOf('.'));

console.log(getFileExt('index.html'));


//3
const moveCharsForward = (str1) => 
   str1
   .split('')
   .map(char => String.fromCharCode(char.charCodeAt(0) + 1))
   .join('');


   console.log(moveCharsForward('EDFHR'));


   //4
const getFullDate = (date = new Date()) => {
   const day = date.getDate();
   const month = date.getMonth() + 1;
   const year = date.getFullYear();
   return `${day}-${month}-${year}`
}

console.log(getFullDate());


//5
const addNew = (str3) => 
str3.indexOf('New!') === 0 ? str3 : `New! ${str3}`;


console.log(addNew('Car'));