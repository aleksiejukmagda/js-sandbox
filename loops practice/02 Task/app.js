//Write a JavaScript conditional statement to sort three numbers.

const numArray = [3, -14, 5, 0, -10, 2, 23, -9];

const sorted = numArray.sort((a,b) => a-b);

console.log(sorted);