const board = document.querySelector('.chessboard');

for(let a = 0; a < 8; a ++) {
   if(a % 2 == 0) {
      const row = document.createElement('div');
      row.className = 'chess__row';
      for(let i = 0; i < 8; i++) {
         const square = document.createElement('div');
         if (i % 2 !== 0){
            square.className = 'chess__item chess__item--dark';
         } else {
            square.className = 'chess__item chess__item--light';
         }
         board.appendChild(row);
         row.appendChild(square);
         board.style.display = 'flex';
      }
   } else {
      const row = document.createElement('div');
      row.className = 'chess__row';
      for(let i = 0; i < 8; i++) {
         const square = document.createElement('div');
         if (i % 2 == 0){
            square.className = 'chess__item chess__item--dark';
         } else {
            square.className = 'chess__item chess__item--light';
         }
         board.appendChild(row);
         row.appendChild(square);
         board.style.display = 'flex';
      }
   }
}


