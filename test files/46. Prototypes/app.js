  // Object.prototype
  // Persno.prototype

  // Person constructor
function Person(firstName, lastName, dob) {
   this.firstName = firstName;
   this.lastName = lastName;
   this.birthday = new Date(dob);
   // this.calculateAge = function(){
   // const diff =  Date.now() - this.birthday.getTime();
   // const ageDate = new Date(diff);
   // return Math.abs(ageDate.getUTCFullYear() - 1970);
   // }
 }

 Person.prototype.calculateAge = function(){
   const diff =  Date.now() - this.birthday.getTime();
   const ageDate = new Date(diff);
   return Math.abs(ageDate.getUTCFullYear() - 1970);
   }
 
Person.prototype.getFullName = function(){
   return `${this.firstName} ${this.lastName}`;
}

Person.prototype.getsMarried = function(newLastName) {
   this.lastName = newLastName;
}

    
 const john = new Person('John', 'Doe', '8-12-90');
 const mary = new Person('Mary', 'Williams', '10-08-99');

 console.log(mary);
 console.log(john.calculateAge());
 console.log(john.getFullName());
 mary.getsMarried('Smith');
 console.log(mary.getFullName());