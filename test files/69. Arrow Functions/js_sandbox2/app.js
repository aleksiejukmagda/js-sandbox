// const sayHello = function() {
//   console.log('Hello');
// }

// const sayHello = () => {
//   console.log('Hello');
// }


//For one line function
// const sayHello = () => console.log('Hello');

//Standard return
// const sayHello = function() {
//    return 'Hello';
// }

//Simpler
// const sayHello = () => 'Hello';


//Return object
// const sayHello = () => ({msg: 'Hello'});

//Single parametr does not need parenthesis
// const sayHello = name => console.log(`Hello ${name}`);

//Multiple params need parenthesis
// const sayHello = (firstName, lastName) => console.log(`Hello ${firstName} ${lastName}`);

// sayHello('Magda', 'Aleksiejuk');


const users = ['Nathan', 'John', 'Aaron'];

// const nameLengths = users.map(function(name) {
// return name.length;
// });


//Shorter
// const nameLengths = users.map((name) => {
// return name.length;
// });

//Shortest
const nameLengths = users.map(name => name.length);

console.log(nameLengths);