// Book Constructor
function Book(title, author, isbn){
   this.title = title;
   this.author = author;
   this.isbn = isbn;
}
//UI Constructor
function UI() {}

UI.prototype.addBookToList = function(book){
   const list = document.getElementById('book-list');
   const row = document.createElement('tr');
   row.innerHTML =`
   <td>${book.title}</td>
   <td>${book.author}</td>
   <td>${book.isbn}</td>
   <td><a href=""class "delete">X</a></td>
   `
}

//EventListeners
document.getElementById('book-form').addEventListener('submit', 
function(e){
   //Get Form Values
   const title = document.getElementById('title').value,
         author = document.getElementById('author').value,
         isbn = document.getElementById('isbn').value;

   //Instantiate book
   const book = new Book(title, author, isbn);

   //Istantiate UI
   const ui = new UI();

   //Add book to the list
   ui.addBookToList(book);

e.preventDefault;
});
